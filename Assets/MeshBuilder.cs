﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshBuilder : MonoBehaviour
{
    void Start()
    {
        Mesh mesh = new Mesh();

        //mesh.vertices = new Vector3[]
        //{
        //    new Vector3(0f, 1f, 0f),
        //    new Vector3(1f, -1f, 0f),
        //    new Vector3(-1f, -1f, 0f),
        //};

        //mesh.uv = new Vector2[]
        //{
        //    new Vector2(0.5f, 0f),
        //    new Vector2(0f, 1f),
        //    new Vector2(1f, 1f)
        //};

        //mesh.triangles = new int[]
        //{
        //    0, 1, 2
        //};

        mesh.vertices = new Vector3[]
        {
            new Vector3(-1f, 1f, 0f),
            new Vector3(1f, 1f, 0f),
            new Vector3(-1f, -1f, 0f),
            new Vector3(1f, -1f, 0f)
        };

        mesh.uv = new Vector2[]
        {
            new Vector2(1f, 0f),
            new Vector2(0f, 0f),
            new Vector2(1f, 1f),
            new Vector2(0f, 1f)
        };

        mesh.triangles = new int[]
        {
            0, 1, 2,
            1, 3, 2
        };

        mesh.RecalculateNormals();

        this.GetComponent<MeshFilter>().mesh = mesh;
    }
}
