﻿Shader "16Dithering"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _NormalMap("Normal Map", 2D) = "black" {}
        _DitherMap("Dither Map", 2D) = "white" {}
        _Alpha("Alpha", Range(0, 1)) = 1
        _Smoothness("Smoothness", Float) = 50
        _DitherSize("Dither Size", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "RenderType" = "Opaque"
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex VertexMain
            #pragma fragment PixelMain

            #include "UnityCG.cginc"

            sampler2D _MainTex;
            sampler2D _NormalMap;
            sampler2D _DitherMap;
            float _Alpha;
            float _Smoothness;
            float _DitherSize;

            struct VertexInput
            {
                float4 position : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 uv : TEXCOORD;
            };

            struct PixelInput
            {
                float4 position : SV_POSITION;
                float3 normal : NORMAL;
                float2 uv : TEXCOORD0;
                float3 viewDirection : TEXCOORD1;
                float3 tspace0 : TEXCOORD2;
                float3 tspace1 : TEXCOORD3;
                float3 tspace2 : TEXCOORD4;
                float4 viewport : TEXCOORD5;
            };

            PixelInput VertexMain(VertexInput input)
            {
                PixelInput output;
                output.position = UnityObjectToClipPos(input.position);
                output.normal = UnityObjectToWorldNormal(input.normal);
                output.uv = input.uv;
                output.viewDirection = mul(unity_ObjectToWorld, input.position).xyz - _WorldSpaceCameraPos;

                float3 tangent = UnityObjectToWorldDir(input.tangent.xyz);
                float tangentSign = input.tangent.w * unity_WorldTransformParams.w;
                float3 bitangent = cross(output.normal, tangent) * tangentSign;
                output.tspace0 = float3(tangent.x, bitangent.x, output.normal.x);
                output.tspace1 = float3(tangent.y, bitangent.y, output.normal.y);
                output.tspace2 = float3(tangent.z, bitangent.z, output.normal.z);

                output.viewport = ComputeScreenPos(output.position);
                return output;
            }

            float3 Lambert(float3 normal, float3 lightDirection)
            {
                float3 l = max(0, dot(normal, -lightDirection)) * 0.5 + 0.5;
                return l * l;
            }

            float3 Specular(float3 normal, float3 lightDirection, float3 viewDirection)
            {
                float3 v = viewDirection;
                float3 r = reflect(-lightDirection, normal);
                return pow(max(0, dot(v, r)), _Smoothness);
            }

            float4 PixelMain(PixelInput input) : SV_TARGET
            {
                float3 normal = normalize(input.normal);
                if (tex2D(_NormalMap, input.uv).a > 0)
                {
                    float3 normalMap = UnpackNormal(tex2D(_NormalMap, input.uv));
                    normal.x = dot(input.tspace0, normalMap);
                    normal.y = dot(input.tspace1, normalMap);
                    normal.z = dot(input.tspace2, normalMap);
                    normal = normalize(normal);
                }

                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 viewDirection = normalize(input.viewDirection);

                float3 color = tex2D(_MainTex, input.uv).rgb;
                float3 light = Lambert(normal, lightDirection) + Specular(normal, lightDirection, viewDirection);

                float2 viewport = input.viewport.xy / input.viewport.w;
                float2 screenPosition = viewport * _ScreenParams.xy;

                if (tex2D(_DitherMap, screenPosition / _DitherSize).r > _Alpha)
                {
                    discard;
                }

                return float4(color * light, 1);
            }
            ENDCG
        }
    }
}
