﻿Shader "01Color"
{
	SubShader
    {
		Tags
        {
            "RenderType" = "Opaque"
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex VertexMain
            #pragma fragment FragmentMain
            
            #include "UnityCG.cginc"
            
            float4 VertexMain(float4 vertexPosition : POSITION) : SV_POSITION
            {
                return UnityObjectToClipPos(vertexPosition);
            }
            
            float4 FragmentMain() : SV_TARGET
            {
                return float4(0, 0, 0, 1);
            }
            ENDCG
        }
	}
}
