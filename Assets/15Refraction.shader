﻿Shader "15Refraction"
{
    Properties
    {
        _NormalMap("Normal Map", 2D) = "black" {}
        _CubeMap("Cube Map", Cube) = "white" {}
        _Refract("Refract", Range(0, 1)) = 0.8
    }

	SubShader
    {
		Tags
        {
            "RenderType" = "Opaque"
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex VertexMain
            #pragma fragment PixelMain
            
            #include "UnityCG.cginc"

            sampler2D _NormalMap;
            samplerCUBE _CubeMap;
            float _Refract;

            struct VertexInput
			{
				float4 position : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
				float2 uv : TEXCOORD;
			};

			struct PixelInput
			{
				float4 position : SV_POSITION;
                float3 normal : NORMAL;
                float2 uv : TEXCOORD0;
                float3 viewDirection : TEXCOORD1;
                float3 tspace0 : TEXCOORD2;
                float3 tspace1 : TEXCOORD3;
                float3 tspace2 : TEXCOORD4;
			};
            
            PixelInput VertexMain(VertexInput input)
            {
				PixelInput output;
                output.position = UnityObjectToClipPos(input.position);
                output.normal = UnityObjectToWorldNormal(input.normal);
				output.uv = input.uv;
                output.viewDirection = mul(unity_ObjectToWorld, input.position).xyz - _WorldSpaceCameraPos;

                float3 tangent = UnityObjectToWorldDir(input.tangent.xyz);
                float tangentSign = input.tangent.w * unity_WorldTransformParams.w;
                float3 bitangent = cross(output.normal, tangent) * tangentSign;
                output.tspace0 = float3(tangent.x, bitangent.x, output.normal.x);
                output.tspace1 = float3(tangent.y, bitangent.y, output.normal.y);
                output.tspace2 = float3(tangent.z, bitangent.z, output.normal.z);

				return output;
            }
            
            float4 Refraction(float3 viewDirection, float3 normal)
            {
                return texCUBE(_CubeMap, refract(viewDirection, normal, _Refract));
            }

            float4 PixelMain(PixelInput input) : SV_TARGET
            {
                float3 normal = normalize(input.normal);
                if (tex2D(_NormalMap, input.uv).a > 0)
                {
                    float3 normalMap = UnpackNormal(tex2D(_NormalMap, input.uv));
                    normal.x = dot(input.tspace0, normalMap);
                    normal.y = dot(input.tspace1, normalMap);
                    normal.z = dot(input.tspace2, normalMap);
                    normal = normalize(normal);
                }

                float3 viewDirection = normalize(input.viewDirection);

                return float4(Refraction(viewDirection, normal).rgb, 1);
            }
            ENDCG
        }
	}
}
