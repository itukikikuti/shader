﻿Shader "07Iterate"
{
	SubShader
    {
		Tags
        {
            "RenderType" = "Opaque"
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex VertexMain
            #pragma fragment PixelMain
            
            #include "UnityCG.cginc"

			struct VertexInput
			{
				float4 position : POSITION;
				float2 uv : TEXCOORD;
			};

			struct PixelInput
			{
				float4 position : SV_POSITION;
				float2 uv : TEXCOORD;
			};
            
            PixelInput VertexMain(VertexInput input)
            {
				PixelInput output;
                output.position = UnityObjectToClipPos(input.position);
				output.uv = input.uv;
				return output;
            }
            
            float4 PixelMain(PixelInput input) : SV_TARGET
            {
                float t = _Time * 30;
				float v = 0;
                for (int i = 0; i < 5; i++)
                {
                    int j = i + 1;
                    float2 orb = float2(cos(t * j), sin(t * j)) * 0.3 + 0.5;
                    v += 0.03 / length(input.uv - orb);
                }
                return float4(v, v, v, 1);
            }
            ENDCG
        }
	}
}
