﻿Shader "04Circle"
{
	SubShader
    {
		Tags
        {
            "RenderType" = "Opaque"
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex VertexMain
            #pragma fragment PixelMain
            
            #include "UnityCG.cginc"

			struct VertexInput
			{
				float4 position : POSITION;
				float2 uv : TEXCOORD;
			};

			struct PixelInput
			{
				float4 position : SV_POSITION;
				float2 uv : TEXCOORD;
			};
            
            PixelInput VertexMain(VertexInput input)
            {
				PixelInput output;
                output.position = UnityObjectToClipPos(input.position);
				output.uv = input.uv;
				return output;
            }
            
            float4 PixelMain(PixelInput input) : SV_TARGET
            {
				float v = length(input.uv - float2(0.5, 0.5));
				if (v < 0.3)
				{
					return float4(1, 1, 1, 1);
				}
                return float4(0, 0, 0, 1);
            }
            ENDCG
        }
	}
}
