﻿Shader "03Rectangle"
{
	SubShader
    {
		Tags
        {
            "RenderType" = "Opaque"
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex VertexMain
            #pragma fragment PixelMain
            
            #include "UnityCG.cginc"

			struct VertexInput
			{
				float4 position : POSITION;
				float2 uv : TEXCOORD;
			};

			struct PixelInput
			{
				float4 position : SV_POSITION;
				float2 uv : TEXCOORD;
			};
            
            PixelInput VertexMain(VertexInput input)
            {
				PixelInput output;
                output.position = UnityObjectToClipPos(input.position);
				output.uv = input.uv;
				return output;
            }
            
            float4 PixelMain(PixelInput input) : SV_TARGET
            {
				if (input.uv.x > 0.2 &&
					input.uv.x < 0.8 &&
					input.uv.y > 0.2 &&
					input.uv.y < 0.8)
				{
					return float4(1, 1, 1, 1);
				}
                return float4(0, 0, 0, 1);
            }
            ENDCG
        }
	}
}
