﻿Shader "09Normal"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
    }

	SubShader
    {
		Tags
        {
            "RenderType" = "Opaque"
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex VertexMain
            #pragma fragment PixelMain
            
            #include "UnityCG.cginc"

            sampler2D _MainTex;

            struct VertexInput
			{
				float4 position : POSITION;
                float3 normal : NORMAL;
				float2 uv : TEXCOORD;
			};

			struct PixelInput
			{
				float4 position : SV_POSITION;
                float3 normal : NORMAL;
                float2 uv : TEXCOORD;
			};
            
            PixelInput VertexMain(VertexInput input)
            {
				PixelInput output;
                output.position = UnityObjectToClipPos(input.position);
                output.normal = UnityObjectToWorldNormal(input.normal);
				output.uv = input.uv;
				return output;
            }
            
            float4 PixelMain(PixelInput input) : SV_TARGET
            {
                return float4(input.normal, 1);
            }
            ENDCG
        }
	}
}
