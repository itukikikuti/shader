﻿Shader "06Time"
{
	SubShader
    {
		Tags
        {
            "RenderType" = "Opaque"
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex VertexMain
            #pragma fragment PixelMain
            
            #include "UnityCG.cginc"

			struct VertexInput
			{
				float4 position : POSITION;
				float2 uv : TEXCOORD;
			};

			struct PixelInput
			{
				float4 position : SV_POSITION;
				float2 uv : TEXCOORD;
			};
            
            PixelInput VertexMain(VertexInput input)
            {
				PixelInput output;
                output.position = UnityObjectToClipPos(input.position);
				output.uv = input.uv;
				return output;
            }
            
            float4 PixelMain(PixelInput input) : SV_TARGET
            {
                float t = _Time * 50;
                float2 orb = float2(cos(t), sin(t)) * 0.3 + 0.5;
				float v = 0.05 / length(input.uv - orb);
                return float4(v, v, v, 1);
            }
            ENDCG
        }
	}
}
