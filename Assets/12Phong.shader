﻿Shader "12Phong"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _Smoothness("Smoothness", Float) = 50
    }

	SubShader
    {
		Tags
        {
            "RenderType" = "Opaque"
        }

        Pass
        {
            CGPROGRAM
            #pragma vertex VertexMain
            #pragma fragment PixelMain
            
            #include "UnityCG.cginc"

            sampler2D _MainTex;
            float _Smoothness;

            struct VertexInput
			{
				float4 position : POSITION;
                float3 normal : NORMAL;
				float2 uv : TEXCOORD;
			};

			struct PixelInput
			{
				float4 position : SV_POSITION;
                float3 normal : NORMAL;
                float2 uv : TEXCOORD0;
                float3 viewDirection : TEXCOORD1;
			};
            
            PixelInput VertexMain(VertexInput input)
            {
				PixelInput output;
                output.position = UnityObjectToClipPos(input.position);
                output.normal = UnityObjectToWorldNormal(input.normal);
				output.uv = input.uv;
                output.viewDirection = mul(unity_ObjectToWorld, input.position).xyz - _WorldSpaceCameraPos;
				return output;
            }
            
            float3 Lambert(float3 normal, float3 lightDirection)
            {
                float3 l = max(0, dot(normal, -lightDirection)) * 0.5 + 0.5;
                return l * l;
            }

            float3 Specular(float3 normal, float3 lightDirection, float3 viewDirection)
            {
                float3 v = viewDirection;
                float3 r = reflect(-lightDirection, normal);
                return pow(max(0, dot(v, r)), _Smoothness);
            }

            float4 PixelMain(PixelInput input) : SV_TARGET
            {
                float3 normal = normalize(input.normal);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 viewDirection = normalize(input.viewDirection);

                float3 color = tex2D(_MainTex, input.uv).rgb;
                float3 light = Lambert(normal, lightDirection) + Specular(normal, lightDirection, viewDirection);
                return float4(color * light, 1);
            }
            ENDCG
        }
	}
}
